ARG BASE_DISTRO=ubuntu
ARG BASE_VERSION=20.04
ARG PYTHON_VERSION=3.10.4
ARG PYTORCH_VERSION=1.13.1
ARG CUDA_VERSION=11.8
ARG HDBET_COMMIT=56c7fb8

FROM registry.gitlab.com/neurobuilds/pytorch:${PYTORCH_VERSION}-py${PYTHON_VERSION}-cuda${CUDA_VERSION}-${BASE_DISTRO}${BASE_VERSION} as build

ARG PYTHON_VERSION
ARG HDBET_COMMIT

RUN apt-get update && \
    apt-get install -y git && \
    rm -rf /var/lib/apt/lists/*

RUN conda install -c defaults -c simpleitk SimpleITK scikit-image && \
    conda clean -ya && \
    pip install git+https://github.com/MIC-DKFZ/HD-BET.git@${HDBET_COMMIT}
RUN echo -e "{\
    \n  \"python_version\": \"$PYTHON_VERSION\" \
    \n  \"cuda_version\": \"$CUDA_VERSION\" \
    \n  \"pytorch_version\": \"$PYTORCH_VERSION\" \
    \n  \"build_version\": \"$HDBET_COMMIT\" \
    \n}" > /opt/conda/manifest.json

COPY paths.py /opt/conda/lib/python${PYTHON_VERSION}/site-packages/HD_BET/
COPY download_data.py /opt
RUN python /opt/download_data.py && \
    rm -f /opt/download_data.py

ENTRYPOINT ['hd-bet']